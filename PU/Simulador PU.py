from tkinter import *

import math

raiz=Tk()

raiz.title("Simulador PU")

raiz.resizable(1,1)

raiz.geometry("1500x600")

raiz.config(bg="black")



miframe=Frame(raiz)

miframe.pack()

miimagen=PhotoImage(file="Sistema para modelar.PNG")

Label(miframe, image=miimagen).grid(row=0, column=0)

miframe.place(x=630,y=460)

miframe.config(bd=10)

miframe.config(relief="raised")

miframe.config(cursor="cross")

miframe.config(width="450",height="240")

#----------------Ingreso de valores --------------

miframe2=Frame(raiz)

miframe2.pack()

miframe2.place(x=10,y=10)

#miframe2.config(bg="gray")

miframe2.config(bd=10)

miframe2.config(relief="groove")

miframe2.config(cursor="arrow")

miframe2.config(width="1500",height="445")

miframe2.grid_propagate(False)



#----------------resultados--------------
miframe3=Frame(raiz)

miframe3.pack()

miframe3.place(x=10,y=460)

#miframe2.config(bg="gray")

miframe3.config(bd=10)

miframe3.config(relief="sunken")

miframe3.config(cursor="arrow")

miframe3.config(width="610",height="240")

miframe3.grid_propagate(False)

def PUBaseChange(zOld, vOld, sOld,zBase):
    zNewPU=(float(zOld)/float(zBase))*((float(vOld)*1000)**2)/(float(sOld)*1000000)    
    return zNewPU
def PerBaseChange(zOld, vOld, sOld,zBase):
    zOld=float(zOld)/100
    zNewPorc=cambioBasePU(zOld, vOld, sOld,zBase)
    return zNewPorc
def ZBaseChange(zOld, zBase):
    zNewOhm=float(zOld)/float(zBase)
    return zNewOhm        


#--------------zetabase zona 1 
def zetaBase():

    
    global multvolbaseZ1
    global multpotbasesys

    global multvolZ2
    global multvolZ3

    global multvolT1Z1
    global multvolT3Z1
    global multvolT2Z5

    global MultimpT1Z1
    global MultimpT2Z5


    multvoltajeT2Z5=1
    multvoltajeT4Z5=1
    multvoltajeG2Z5=1
    multpotenciaG2Z5=1
    multpotenciaT2Z5=1
    multimpedanciaG2Z5=1
    multimpedanciaT2Z5=1
    multimpedanciaT4Z5=1


    multpotenciabasesys=1
    multvoltajebaseZ1=1
    multvoltajeT1Z1=1
    multvoltajeT3Z1=1
    multvoltajeG1Z1=1
    multpotenciaG1Z1=1
    multpotenciaT1Z1=1
    multpotenciaT3Z1=1
    multimpedanciaG1Z1=1
    multimpedanciaT1Z1=1
    multimpedanciaT3Z1=1

    multvoltajeT5Z6=1
    multvoltajeM1Z6=1
    multpotenciaM1Z6=1
    multpotenciaT5Z6=1
    multimpedanciaM1Z6=1
    multimpedanciaT5Z6=1

   
    
    if multvolbaseZ1.get() == "MV":
        multvoltajebaseZ1= 1000000
    
    elif  multvolbaseZ1.get() == "KV":
            multvoltajebaseZ1= 1000

    elif  multvolbaseZ1.get() == "V":
           multvoltajebaseZ1= 1


    if multpotbasesys.get() == "MVA":
        multpotenciabasesys= 1000000
    
    elif  multpotbasesys.get() == "KVA":
            multpotenciabasesys= 1000

    elif  multpotbasesys.get() == "VA":
             multpotenciabasesys= 1
    
    
    Ibasez1=(((float(tensionZ1.get())*multvoltajebaseZ1)**2)/(float(potenciaZ1.get())*multpotenciabasesys))

    
    resultimpbasez1.set(((float(tensionZ1.get())*multvoltajebaseZ1)**2)/(float(potenciaZ1.get())*multpotenciabasesys))

    
    


    ######## tension e impedancia base zona2 


    if multvolT1Z1.get() == "MV":
        multvoltajeT1Z1= 1000000
    
    elif  multvolT1Z1.get() == "KV":
            multvoltajeT1Z1= 1000

    elif  multvolT1Z1.get() == "V":
           multvoltajeT1Z1= 1
    
    if multpotT1Z1.get() == "MVA":
        multpotenciaT1Z1= 1000000
    
    elif  multvolT1Z1.get() == "KVA":
            multpotenciaT1Z1= 1000

    elif  multvolT1Z1.get() == "VA":
           multpotenciaT1Z1= 1

    multvoltajeL1Z2=1

    if multvolZ2.get() == "MV":
        multvoltajeL1Z2= 1000000
    
    elif  multvolZ2.get() == "KV":
            multvoltajeL1Z2= 1000

    elif  multvolZ2.get() == "V":
           multvoltajeL1Z2= 1

    reltrafo1=(float(T1VZ1.get())*multvoltajeT1Z1)/(float(L1VZ2.get())*multvoltajeL1Z2)

    resulttensionbasez2.set(((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo1 ))

    Ibasez2=(((((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo1 ))**2)/(float(potenciaZ1.get())*multpotenciabasesys))

    resultimpbasez2.set(((((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo1 ))**2)/(float(potenciaZ1.get())*multpotenciabasesys))



    ######## tension e impedancia base zona3

    if multvolT3Z1.get() == "MV":
        multvoltajeT3Z1= 1000000
    
    elif  multvolT3Z1.get() == "KV":
            multvoltajeT3Z1= 1000

    elif  multvolT3Z1.get() == "V":
           multvoltajeT3Z1= 1

    if multpotT3Z1.get() == "MVA":
        multpotenciaT3Z1= 1000000
    
    elif  multvolT3Z1.get() == "KVA":
            multpotenciaT3Z1= 1000

    elif  multvolT3Z1.get() == "VA":
           multpotenciaT3Z1= 1

    multvoltajeL2Z3=1

    if multvolZ3.get() == "MV":
        multvoltajeL2Z3= 1000000
    
    elif  multvolZ3.get() == "KV":
            multvoltajeL2Z3= 1000

    elif  multvolZ3.get() == "V":
           multvoltajeL2Z3= 1

    reltrafo3=(float(T3VZ1.get())*multvoltajeT3Z1)/(float(L2VZ3.get())*multvoltajeL2Z3)

    resulttensionbasez3.set(((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo3 ))

    Ibasez3=(((((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo3 ))**2)/(float(potenciaZ1.get())*multpotenciabasesys))

    resultimpbasez3.set(((((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo3 ))**2)/(float(potenciaZ1.get())*multpotenciabasesys))

    ######## tension e impedancia base zona4--------------------

    resulttensionbasez4.set(((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo3 ))

    Ibasez4=(((((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo3 ))**2)/(float(potenciaZ1.get())*multpotenciabasesys))
     
    resultimpbasez4.set(((((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo3 ))**2)/(float(potenciaZ1.get())*multpotenciabasesys))
     
    ######## tension e impedancia base zona5--------------------
    

    if multvolT2Z5.get() == "MV":
        multvoltajeT2Z5= 1000000
    
    elif  multvolT2Z5.get() == "KV":
            multvoltajeT2Z5= 1000

    elif  multvolT2Z5.get() == "V":
           multvoltajeT2Z5= 1

    
    if multvolT4Z5.get() == "MV":
        multvoltajeT4Z5= 1000000
    
    elif  multvolT4Z5.get() == "KV":
            multvoltajeT4Z5= 1000

    elif  multvolT4Z5.get() == "V":
            multvoltajeT4Z5= 1


    if multpotT2Z5.get() == "MVA":
        multpotenciaT2Z5= 1000000
    
    elif  multvolT2Z5.get() == "KVA":
            multpotenciaT2Z5= 1000

    elif  multvolT2Z5.get() == "VA":
           multpotenciaT2Z5= 1

    if multpotT4Z5.get() == "MVA":
        multpotenciaT4Z5= 1000000
    
    elif  multvolT4Z5.get() == "KVA":
            multpotenciaT4Z5= 1000

    elif  multvolT4Z5.get() == "VA":
           multpotenciaT4Z5= 1

    reltrafo2=(float(T2VZ5.get())*multvoltajeT2Z5)/(float(L1VZ2.get())*multvoltajeL1Z2)
     
    resulttensionbasez5.set( (((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo2 ))*reltrafo2 )

    Ibasez5=((( (((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo2 ))*reltrafo2 )**2)/(float(potenciaZ1.get())*multpotenciabasesys))
    
    resultimpbasez5.set((( (((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo2 ))*reltrafo2 )**2)/(float(potenciaZ1.get())*multpotenciabasesys))
    
    # ######### tension e impedancia base zona6------------------
    
    if multvolT5Z6.get() == "MV":
        multvoltajeT5Z6= 1000000
    
    elif  multvolT5Z6.get() == "KV":
            multvoltajeT5Z6= 1000

    elif  multvolT5Z6.get() == "V":
           multvoltajeT5Z6= 1

    
    if multvol2T5Z6.get() == "MV":
        multvoltaje2T5Z6= 1000000
    
    elif  multvol2T5Z6.get() == "KV":
            multvoltaje2T5Z6= 1000

    elif  multvol2T5Z6.get() == "V":
           multvoltaje2T5Z6= 1


    if multpotT5Z6.get() == "MVA":
        multpotenciaT5Z6= 1000000
    
    elif  multvolT5Z6.get() == "KVA":
            multpotenciaT5Z6= 1000

    elif  multvolT5Z6.get() == "VA":
           multpotenciaT5Z6= 1


    reltrafo5=(float(T5VZ6.get())*multvoltajeT5Z6)/(float(T5V2Z6.get())*multvoltaje2T5Z6)### cambiar la tension de la linea por la correcta 
    
    resulttensionbasez6.set( (((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo3 ))*reltrafo5 )

    Ibasez6=((( (((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo3 ))*reltrafo5 )**2)/(float(potenciaZ1.get())*multpotenciabasesys))
     
    resultimpbasez6.set((( (((float(tensionZ1.get())*multvoltajebaseZ1)*reltrafo3 ))*reltrafo5 )**2)/(float(potenciaZ1.get())*multpotenciabasesys)) 
     
     
    # ----------- impedancia transformador 1 -----------
    
    
    
    ZoldT1= math.sqrt((float(T1RZZ1.get())**2)+(float(T1IZZ1.get())**2))
    

    if  MultimpT1Z1.get() == "PU":
        #zNewPUG=cambioBasePU(float(T1RZZ1.get()),float(T1VZ1.get()),float(T1SZ1.get()),Ibasez1)
        #znewPUT1=(float(T1RZZ1.get())/Ibasez1)*((float(T1VZ1.get())*multvoltajeT1Z1)**2)/(float(T1SZ1.get())*multpotenciaT1Z1)    
        #zNewPU=(float(zOld)/float(zBase))*((float(vOld)*1000)**2)/(float(sOld)*1000000)    
        resulimpT1.set((float(T1RZZ1.get())/Ibasez1)*((float(T1VZ1.get())*multvoltajeT1Z1)**2)/(float(T1SZ1.get())*multpotenciaT1Z1)  )
        
    elif MultimpT1Z1.get()=="%":
        
        resulimpT1.set((float(T1RZZ1.get())/(Ibasez1*100))*((float(T1VZ1.get())*multvoltajeT1Z1)**2)/(float(T1SZ1.get())*multpotenciaT1Z1)  )

    elif MultimpT1Z1.get()=="kΩ":
        ZoldT1=ZoldT1*1000
        resulimpT1.set(ZoldT1/float(Ibasez1))

    elif MultimpT1Z1.get() =="Ω":

       resulimpT1.set(ZoldT1/float(Ibasez1))

    elif MultimpT1Z1.get()=="mΩ":
        ZoldT1=ZoldT1/1000
        resulimpT1.set(ZoldT1/float(Ibasez1))
        
        
        
    #---------- impedancia transformador 2 -----------
    
    ZoldT2= math.sqrt((float(T2RZZ5.get())**2)+(float(T2IZZ5.get())**2))
    

    if  MultimpT2Z5.get() == "PU":
        #zNewPUG=cambioBasePU(float(T1RZZ1.get()),float(T1VZ1.get()),float(T1SZ1.get()),Ibasez1)
        #znewPUT1=(float(T1RZZ1.get())/Ibasez1)*((float(T1VZ1.get())*multvoltajeT1Z1)**2)/(float(T1SZ1.get())*multpotenciaT1Z1)    
        #zNewPU=(float(zOld)/float(zBase))*((float(vOld)*1000)**2)/(float(sOld)*1000000)    
        resulimpT2.set((float(T2RZZ5.get())/Ibasez5)*((float(T2SZ5.get())*multvoltajeT2Z5)**2)/(float(T2SZ5.get())*multpotenciaT2Z5)  )
        
    elif MultimpT2Z5.get()=="%":
        
        resulimpT2.set((float(T2RZZ5.get())/(Ibasez5*100))*((float(T2SZ5.get())*multvoltajeT2Z5)**2)/(float(T2SZ5.get())*multpotenciaT2Z5)  )

    elif MultimpT2Z5.get()=="KΩ":
        ZoldT2=ZoldT2*1000
        resulimpT2.set(ZoldT2/float(Ibasez5))

    elif MultimpT2Z5.get() =="Ω":

       resulimpT2.set(ZoldT2/float(Ibasez5))

    elif MultimpT2Z5.get()=="mΩ":
        ZoldT2=ZoldT2/1000
        resulimpT2.set(ZoldT2/float(Ibasez5))
    

    #---------- impedancia transformador 3 -----------
    
    ZoldT3= math.sqrt((float(T3RZZ1.get())**2)+(float(T3IZZ1.get())**2))
    

    if  MultimpT3Z1.get() == "PU":
        #zNewPUG=cambioBasePU(float(T1RZZ1.get()),float(T1VZ1.get()),float(T1SZ1.get()),Ibasez1)
        #znewPUT1=(float(T1RZZ1.get())/Ibasez1)*((float(T1VZ1.get())*multvoltajeT1Z1)**2)/(float(T1SZ1.get())*multpotenciaT1Z1)    
        #zNewPU=(float(zOld)/float(zBase))*((float(vOld)*1000)**2)/(float(sOld)*1000000)    
        resulimpT2.set((float(T3RZZ1.get())/Ibasez1)*((float(T3VZ1.get())*multvoltajeT3Z1)**2)/(float(T3SZ1.get())*multpotenciaT3Z1)  )
        
    elif MultimpT3Z1.get()=="%":
        
        resulimpT3.set((float(T3RZZ1.get())/(Ibasez1*100))*((float(T3VZ1.get())*multvoltajeT3Z1)**2)/(float(T3SZ1.get())*multpotenciaT3Z1)  )

    elif MultimpT3Z1.get()=="KΩ":
        ZoldT3=ZoldT3*1000
        resulimpT3.set(ZoldT3/float(Ibasez1))

    elif MultimpT3Z1.get() =="Ω":

       resulimpT3.set(ZoldT3/float(Ibasez1))

    elif MultimpT3Z1.get()=="mΩ":
        ZoldT3=ZoldT3/1000
        resulimpT3.set(ZoldT3/float(Ibasez1))

    #---------- impedancia transformador 4 -----------
    
    ZoldT4= math.sqrt((float(T4RZZ5.get())**2)+(float(T4IZZ5.get())**2))
    

    if  MultimpT4Z5.get() == "PU":
        #zNewPUG=cambioBasePU(float(T1RZZ1.get()),float(T1VZ1.get()),float(T1SZ1.get()),Ibasez1)
        #znewPUT1=(float(T1RZZ1.get())/Ibasez1)*((float(T1VZ1.get())*multvoltajeT1Z1)**2)/(float(T1SZ1.get())*multpotenciaT1Z1)    
        #zNewPU=(float(zOld)/float(zBase))*((float(vOld)*1000)**2)/(float(sOld)*1000000)    
        resulimpT4.set((float(T4RZZ5.get())/Ibasez5)*((float(T4VZ5.get())*multvoltajeT4Z5)**2)/(float(T4SZ5.get())*multpotenciaT4Z5)  )
        
    elif MultimpT4Z5.get()=="%":
        
        resulimpT4.set((float(T4RZZ5.get())/(Ibasez5*100))*((float(T4VZ5.get())*multvoltajeT4Z5)**2)/(float(T3SZ1.get())*multpotenciaT3Z1)  )

    elif MultimpT4Z5.get()=="KΩ":
        ZoldT4=ZoldT4*1000
        resulimpT4.set(ZoldT4/float(Ibasez5))

    elif MultimpT4Z5.get() =="Ω":

       resulimpT4.set(ZoldT4/float(Ibasez5))

    elif MultimpT4Z5.get()=="mΩ":
        ZoldT4=ZoldT4/1000
        resulimpT4.set(ZoldT4/float(Ibasez5))


    #---------- impedancia transformador 5-----------
    
    ZoldT5= math.sqrt((float(T5RZZ6.get())**2)+(float(T5IZZ6.get())**2))
    

    if  MultimpT5Z6.get() == "PU":
        #zNewPUG=cambioBasePU(float(T1RZZ1.get()),float(T1VZ1.get()),float(T1SZ1.get()),Ibasez1)
        #znewPUT1=(float(T1RZZ1.get())/Ibasez1)*((float(T1VZ1.get())*multvoltajeT1Z1)**2)/(float(T1SZ1.get())*multpotenciaT1Z1)    
        #zNewPU=(float(zOld)/float(zBase))*((float(vOld)*1000)**2)/(float(sOld)*1000000)    
        resulimpT5.set((float(T5RZZ6.get())/Ibasez6)*((float(T5VZ6.get())*multvoltajeT5Z6)**2)/(float(T5SZ6.get())*multpotenciaT5Z6)  )
        
    elif MultimpT5Z6.get()=="%":
        
        resulimpT5.set((float(T5RZZ6.get())/(Ibasez6*100))*((float(T5VZ6.get())*multvoltajeT5Z6)**2)/(float(T5SZ6.get())*multpotenciaT5Z6)  )

    elif MultimpT5Z6.get()=="KΩ":
        ZoldT5=ZoldT5*1000
        resulimpT5.set(ZoldT5/float(Ibasez6))

    elif MultimpT5Z6.get() =="Ω":

       resulimpT5.set(ZoldT5/float(Ibasez6))

    elif MultimpT5Z6.get()=="mΩ":
        ZoldT5=ZoldT5/1000
        resulimpT5.set(ZoldT5/float(Ibasez6))

    


    # ----------- impedancia generador 1 -----------

    if multvolG1Z1.get() == "MV":
        multvoltajeT1Z1= 1000000
    
    elif  multvolG1Z1.get() == "KV":
            multvoltajeG1Z1= 1000

    elif  multvolG1Z1.get() == "V":
           multvoltajeG1Z1= 1
    
    if multpotG1Z1.get() == "MVA":
        multpotenciaG1Z1= 1000000
    
    elif  multvolG1Z1.get() == "KVA":
            multpotenciaG1Z1= 1000

    elif  multvolG1Z1.get() == "VA":
           multpotenciaG1Z1= 1



    ZoldG1= math.sqrt((float(G1RZZ1.get())**2)+(float(G1IZZ1.get())**2))
    

    if  MultimpG1Z1.get() == "PU":
        #zNewPUG=cambioBasePU(float(T1RZZ1.get()),float(T1VZ1.get()),float(T1SZ1.get()),Ibasez1)
        #znewPUT1=(float(T1RZZ1.get())/Ibasez1)*((float(T1VZ1.get())*multvoltajeT1Z1)**2)/(float(T1SZ1.get())*multpotenciaT1Z1)    
        #zNewPU=(float(zOld)/float(zBase))*((float(vOld)*1000)**2)/(float(sOld)*1000000)    
        resulimpG1.set((float(G1RZZ1.get())/Ibasez1)*((float(G1VZ1.get())*multvoltajeG1Z1)**2)/(float(G1SZ1.get())*multpotenciaG1Z1)  )
        
    elif MultimpG1Z1.get()=="%":
        
        resulimpG1.set((float(G1RZZ1.get())/(Ibasez1*100))*((float(G1VZ1.get())*multvoltajeG1Z1)**2)/(float(G1SZ1.get())*multpotenciaG1Z1)  )

    elif MultimpG1Z1.get()=="KΩ":
        ZoldG1=ZoldG1*1000
        resulimpG1.set(ZoldG1/float(Ibasez1))

    elif MultimpG1Z1.get() =="Ω":

       resulimpG1.set(ZoldG1/float(Ibasez1))

    elif MultimpG1Z1.get()=="mΩ":
        ZoldG1=ZoldG1/1000
        resulimpG1.set(ZoldG1/float(Ibasez1))



    # ----------- impedancia generador 2 -----------

    if multvolG2Z5.get() == "MV":
        multvoltajeG2Z5= 1000000
    
    elif  multvolG2Z5.get() == "KV":
            multvoltajeG2Z5= 1000

    elif  multvolG2Z5.get() == "V":
           multvoltajeG2Z5= 1
    
    if multpotG2Z5.get() == "MVA":
        multpotenciaG2Z5= 1000000
    
    elif  multvolG2Z5.get() == "KVA":
            multpotenciaG2Z5= 1000

    elif  multvolG2Z5.get() == "VA":
           multpotenciaG2Z5= 1



    ZoldG2= math.sqrt((float(G2RZZ5.get())**2)+(float(G2IZZ5.get())**2))
    

    if  MultimpG2Z5.get() == "PU":
        #zNewPUG=cambioBasePU(float(T1RZZ1.get()),float(T1VZ1.get()),float(T1SZ1.get()),Ibasez1)
        #znewPUT1=(float(T1RZZ1.get())/Ibasez1)*((float(T1VZ1.get())*multvoltajeT1Z1)**2)/(float(T1SZ1.get())*multpotenciaT1Z1)    
        #zNewPU=(float(zOld)/float(zBase))*((float(vOld)*1000)**2)/(float(sOld)*1000000)    
        resulimpG2.set((float(G2RZZ5.get())/Ibasez5)*((float(G2VZ5.get())*multvoltajeG2Z5)**2)/(float(G2SZ5.get())*multpotenciaG2Z5)  )
        
    elif MultimpG2Z5.get()=="%":
        
        resulimpG2.set((float(G2RZZ5.get())/(Ibasez5*100))*((float(G2VZ5.get())*multvoltajeG2Z5)**2)/(float(G2SZ5.get())*multpotenciaG2Z5)  )

    elif MultimpG2Z5.get()=="KΩ":
        ZoldG2=ZoldG2*1000
        resulimpG2.set(ZoldG2/float(Ibasez5))

    elif MultimpG2Z5.get() =="Ω":

       resulimpG2.set(ZoldG2/float(Ibasez5))

    elif MultimpG2Z5.get()=="mΩ":
        ZoldG2=ZoldG2/1000
        resulimpG2.set(ZoldG2/float(Ibasez5))


    # ----------- impedancia motor-----------

    if multvolM1Z6.get() == "MV":
        multvoltajeM1Z6= 1000000
    
    elif  multvolM1Z6.get() == "KV":
            multvoltajeM1Z6= 1000

    elif  multvolM1Z6.get() == "V":
           multvoltajeM1Z6= 1
    
    if multpotM1Z6.get() == "MVA":
        multpotenciaM1Z6= 1000000
    
    elif  multvolM1Z6.get() == "KVA":
            multpotenciaM1Z6= 1000

    elif  multvolM1Z6.get() == "VA":
           multpotenciaM1Z6= 1



    ZoldM= math.sqrt((float(M1RZZ6.get())**2)+(float(M1IZZ6.get())**2))
    

    if  MultimpM1Z6.get() == "PU":
        #zNewPUG=cambioBasePU(float(T1RZZ1.get()),float(T1VZ1.get()),float(T1SZ1.get()),Ibasez1)
        #znewPUT1=(float(T1RZZ1.get())/Ibasez1)*((float(T1VZ1.get())*multvoltajeT1Z1)**2)/(float(T1SZ1.get())*multpotenciaT1Z1)    
        #zNewPU=(float(zOld)/float(zBase))*((float(vOld)*1000)**2)/(float(sOld)*1000000)    
        resulimpM1.set((float(M1RZZ6.get())/Ibasez6)*((float(M1VZ6.get())*multvoltajeM1Z6)**2)/(float(M1SZ6.get())*multpotenciaM1Z6)  )
        
    elif MultimpM1Z6.get()=="%":
        
        resulimpM1.set((float(M1RZZ6.get())/(Ibasez6*100))*((float(M1VZ6.get())*multvoltajeM1Z6)**2)/(float(M1SZ6.get())*multpotenciaM1Z6)  )

    elif MultimpM1Z6.get()=="KΩ":
        ZoldM=ZoldM*1000
        resulimpM1.set(ZoldM/float(Ibasez6))

    elif MultimpM1Z6.get() =="Ω":

       resulimpM1.set(ZoldM/float(Ibasez6))

    elif MultimpM1Z6.get()=="mΩ":
        ZoldM=ZoldM/1000
        resulimpM1.set(ZoldM/float(Ibasez6))


    



    # ----------- impedancia L1 -----------

    ZoldL1= math.sqrt((float(L1RZZ1.get())**2)+(float(L1IZZ1.get())**2))

    if MultimpZ2.get()=="KΩ":
        
        resulimpL1.set(ZoldL1*1000/float(Ibasez2))

    elif MultimpZ2.get() =="Ω":

       resulimpL1.set(ZoldT1/float(Ibasez2))

    elif MultimpZ2.get()=="mΩ":
        resulimpL1.set((ZoldT1/1000)/float(Ibasez2))


    # ----------- impedancia L2 -----------
    ZoldL2= math.sqrt((float(L2RZZ3.get())**2)+(float(L2IZZ3.get())**2))

    if MultimpZ3.get()=="KΩ":
        
        resulimpL2.set(ZoldL2*1000/float(Ibasez3))

    elif MultimpZ3.get() =="Ω":

       resulimpL2.set(ZoldL2/float(Ibasez3))

    elif MultimpZ3.get()=="mΩ":
        resulimpL2.set((ZoldL2/1000)/float(Ibasez3))


    




    # ----------- impedancia L3 -----------
    ZoldL3= math.sqrt((float(L3RZZ4.get())**2)+(float(L3IZZ4.get())**2))

    if MultimpZ4.get()=="KΩ":
        
        resulimpL3.set(ZoldL3*1000/float(Ibasez4))

    elif MultimpZ4.get() =="Ω":

       resulimpL3.set(ZoldL3/float(Ibasez4))

    elif MultimpZ4.get()=="mΩ":
        resulimpL3.set((ZoldL3/1000)/float(Ibasez4))

    # #



    

    
        
    
    
    
    

#------------------------------zona 1---------- ############inicio interfaz ##########################################################################################################

zon1=Label(miframe2, text="ZONA 1:")
zon1.grid(row=0, column=2)
zon1.config(fg="black",font=("Verdana",8))

tensionlabel=Label(miframe2, text="Tension base:")
tensionlabel.grid(row=1, column=1)


generador1=Label(miframe2, text="Generador 1:")
generador1.grid(row=2, column=1)

tlabel=Label(miframe2, text=" S")
tlabel.grid(row=3, column=1)

tlabel=Label(miframe2, text=" V")
tlabel.grid(row=4, column=1)

tlabel=Label(miframe2, text=" Z")
tlabel.grid(row=5, column=1)

tlabel34=Label(miframe2, text=" +j")
tlabel34.place(x=160,y=150)
#tlabel.grid(row=5, column=3)

transformador1=Label(miframe2, text="Transformador 1:")
transformador1.grid(row=6, column=1)

tlabel=Label(miframe2, text=" S")
tlabel.grid(row=7, column=1)

tlabel=Label(miframe2, text=" V")
tlabel.grid(row=8, column=1)

tlabel=Label(miframe2, text=" Z")
tlabel.grid(row=9, column=1)

tlabel34=Label(miframe2, text=" +j")
tlabel34.place(x=160,y=273)


transformador3=Label(miframe2, text="Transformador 3:")
transformador3.grid(row=10, column=1)

tlabel=Label(miframe2, text=" S")
tlabel.grid(row=11, column=1)

tlabel=Label(miframe2, text=" V")
tlabel.grid(row=12, column=1)

tlabel=Label(miframe2, text=" Z")
tlabel.grid(row=13, column=1)

tlabel34=Label(miframe2, text=" +j")
tlabel34.place(x=160,y=398)



varsis1=Label(miframe2, text="Variables sistema:")
varsis1.grid(row=11, column=14)

tensionlabel=Label(miframe2, text="S sistema:")
tensionlabel.grid(row=12, column=13)

tensionZ1=StringVar()
potenciaZ1=StringVar()


G1SZ1=StringVar()
G1VZ1=StringVar()
G1RZZ1=StringVar()
G1IZZ1=StringVar()


T1SZ1=StringVar()
T1VZ1=StringVar()
T1RZZ1=StringVar()
T1IZZ1=StringVar()

T3SZ1=StringVar()
T3VZ1=StringVar()
T3RZZ1=StringVar()
T3IZZ1=StringVar()

pantalla=Entry(miframe2, textvariable=tensionZ1,width=4)
pantalla.grid(row=1, column=2, padx=1, pady=1, columnspan=1)
pantalla.config(fg="black", justify="right")

pantalla2=Entry(miframe2, textvariable=potenciaZ1,width=4)
pantalla2.grid(row=12, column=14, padx=1, pady=1, columnspan=1)
pantalla2.config(fg="black", justify="right")

pantalla3=Entry(miframe2, textvariable=G1SZ1,width=4)
pantalla3.grid(row=3, column=2, padx=1, pady=1, columnspan=1)
pantalla3.config(fg="black", justify="right")

pantalla4=Entry(miframe2, textvariable=G1VZ1,width=4)
pantalla4.grid(row=4, column=2, padx=1, pady=1, columnspan=1)
pantalla4.config(fg="black", justify="right")

pantalla5=Entry(miframe2, textvariable=G1RZZ1,width=4)
pantalla5.grid(row=5, column=2, padx=1, pady=1, columnspan=1)
pantalla5.config(fg="black", justify="right")

pantalla6=Entry(miframe2, textvariable=G1IZZ1,width=4)
pantalla6.grid(row=5, column=3, padx=20, pady=1, columnspan=1)
pantalla6.config(fg="black", justify="right")


pantalla3=Entry(miframe2, textvariable=T1SZ1,width=4)
pantalla3.grid(row=7, column=2, padx=1, pady=1, columnspan=1)
pantalla3.config(fg="black", justify="right")

pantalla4=Entry(miframe2, textvariable=T1VZ1,width=4)
pantalla4.grid(row=8, column=2, padx=1, pady=1, columnspan=1)
pantalla4.config(fg="black", justify="right")

pantalla5=Entry(miframe2, textvariable=T1RZZ1,width=4)
pantalla5.grid(row=9, column=2, padx=1, pady=1, columnspan=1)
pantalla5.config(fg="black", justify="right")

pantalla6=Entry(miframe2, textvariable=T1IZZ1,width=4)
pantalla6.grid(row=9, column=3, padx=1, pady=1, columnspan=1)
pantalla6.config(fg="black", justify="right")



pantalla3=Entry(miframe2, textvariable=T3SZ1,width=4)
pantalla3.grid(row=11, column=2, padx=1, pady=1, columnspan=1)
pantalla3.config(fg="black", justify="right")

pantalla4=Entry(miframe2, textvariable=T3VZ1,width=4)
pantalla4.grid(row=12, column=2, padx=1, pady=1, columnspan=1)
pantalla4.config(fg="black", justify="right")

pantalla5=Entry(miframe2, textvariable=T3RZZ1,width=4)
pantalla5.grid(row=13, column=2, padx=1, pady=1, columnspan=1)
pantalla5.config(fg="black", justify="right")

pantalla6=Entry(miframe2, textvariable=T3IZZ1,width=4)
pantalla6.grid(row=13, column=3, padx=1, pady=1, columnspan=1)
pantalla6.config(fg="black", justify="right")





multtensiones=["MV","KV","V"]
multpotencias=["MVA","KVA","VA"]
multiimpedancias=["KΩ","Ω","mΩ","PU","%"]
multiimpedanciasLINEAS=["KΩ","Ω","mΩ"]

multpotbasesys=StringVar(raiz)
multvolbaseZ1=StringVar(raiz)
multpotG1Z1=StringVar(raiz)
multpotT1Z1=StringVar(raiz)
multpotT3Z1=StringVar(raiz)
multvolG1Z1=StringVar(raiz)
multvolT1Z1=StringVar(raiz)
multvolT3Z1=StringVar(raiz)

MultimpG1Z1=StringVar(raiz)
MultimpT1Z1=StringVar(raiz)
MultimpT3Z1=StringVar(raiz)






opcion=OptionMenu(miframe2,multvolbaseZ1,*multtensiones)
opcion.config(width=2)
opcion.grid(row=1, column=3)

opcion2=OptionMenu(miframe2,multpotbasesys,*multpotencias)
opcion2.config(width=2)
opcion2.grid(row=12, column=15)



opcion2=OptionMenu(miframe2,multpotG1Z1,*multpotencias)
opcion2.config(width=2)
opcion2.grid(row=3, column=3)

opcion2=OptionMenu(miframe2,multpotT1Z1,*multpotencias)
opcion2.config(width=2)
opcion2.grid(row=7, column=3)

opcion2=OptionMenu(miframe2,multpotT3Z1,*multpotencias)
opcion2.config(width=2)
opcion2.grid(row=11, column=3)



opcion3=OptionMenu(miframe2,multvolG1Z1,*multtensiones)
opcion3.config(width=2)
opcion3.grid(row=4, column=3)

opcion3=OptionMenu(miframe2,multvolT1Z1,*multtensiones)
opcion3.config(width=2)
opcion3.grid(row=8, column=3)

opcion3=OptionMenu(miframe2,multvolT3Z1,*multtensiones)
opcion3.config(width=2)
opcion3.grid(row=12, column=3)



opcion3=OptionMenu(miframe2,MultimpG1Z1,*multiimpedancias)
opcion3.config(width=2)
opcion3.grid(row=5, column=4, padx=1)

opcion3=OptionMenu(miframe2,MultimpT1Z1,*multiimpedancias)
opcion3.config(width=2)
opcion3.grid(row=9, column=4, padx=1)

opcion3=OptionMenu(miframe2,MultimpT3Z1,*multiimpedancias)
opcion3.config(width=2)
opcion3.grid(row=13, column=4, padx=1)




#----------------------zona 2--------------------------------------------------------------------------------------------------------------------------

zona2=Label(miframe2, text="ZONA 2:")
zona2.grid(row=0, column=6)
zona2.config(fg="black",font=("Verdana",8))

tensionlabel=Label(miframe2, text="Linea 1:")
tensionlabel.grid(row=1, column=5, padx=10)

tensionlabel=Label(miframe2, text="V:")
tensionlabel.grid(row=2, column=5, padx=1)

tensionlabel=Label(miframe2, text="Z:")
tensionlabel.grid(row=3, column=5, padx=1)

tlabel34=Label(miframe2, text=" +j")
tlabel34.place(x=473,y=88)

zona3=Label(miframe2, text="ZONA 3:")
zona3.grid(row=4, column=6)
zona3.config(fg="black",font=("Verdana",10))

tensionlabel=Label(miframe2, text="Linea 2:")
tensionlabel.grid(row=5, column=5, padx=1)

tensionlabel=Label(miframe2, text="V:")
tensionlabel.grid(row=6, column=5, padx=1)

tensionlabel=Label(miframe2, text="Z:")
tensionlabel.grid(row=7, column=5, padx=1)

tlabel34=Label(miframe2, text=" +j")
tlabel34.place(x=473,y=210)


zona1=Label(miframe2, text="ZONA 4:")
zona1.grid(row=8, column=6)
zona1.config(fg="black",font=("Verdana",8))

tensionlabel=Label(miframe2, text="Linea 3:")
tensionlabel.grid(row=9, column=5, padx=1)

tensionlabel=Label(miframe2, text="V:")
tensionlabel.grid(row=10, column=5)

tensionlabel=Label(miframe2, text="Z:")
tensionlabel.grid(row=11, column=5)

tlabel34=Label(miframe2, text=" +j")
tlabel34.place(x=473,y=333)


L1VZ2=StringVar()
L1RZZ1=StringVar()
L1IZZ1=StringVar()

L2VZ3=StringVar()
L2RZZ3=StringVar()
L2IZZ3=StringVar()

L3VZ4=StringVar()
L3RZZ4=StringVar()
L3IZZ4=StringVar()


pantalla=Entry(miframe2, textvariable=L1VZ2,width=4)
pantalla.grid(row=2, column=6, padx=1, pady=1, columnspan=1)
pantalla.config(fg="black", justify="right")

pantalla2=Entry(miframe2, textvariable=L1RZZ1,width=4)
pantalla2.grid(row=3, column=6, padx=1, pady=1, columnspan=1)
pantalla2.config(fg="black", justify="right")

pantalla3=Entry(miframe2, textvariable=L1IZZ1,width=4)
pantalla3.grid(row=3, column=7, padx=20, pady=1, columnspan=1)
pantalla3.config(fg="black", justify="right")



pantalla=Entry(miframe2, textvariable=L2VZ3,width=4)
pantalla.grid(row=6, column=6, padx=1, pady=1, columnspan=1)
pantalla.config(fg="black", justify="right")

pantalla2=Entry(miframe2, textvariable=L2RZZ3,width=4)
pantalla2.grid(row=7, column=6, padx=1, pady=1, columnspan=1)
pantalla2.config(fg="black", justify="right")

pantalla3=Entry(miframe2, textvariable=L2IZZ3,width=4)
pantalla3.grid(row=7, column=7, padx=1, pady=1, columnspan=1)
pantalla3.config(fg="black", justify="right")


pantalla=Entry(miframe2, textvariable=L3VZ4,width=4)
pantalla.grid(row=10, column=6, padx=1, pady=1, columnspan=1)
pantalla.config(fg="black", justify="right")

pantalla2=Entry(miframe2, textvariable=L3RZZ4,width=4)
pantalla2.grid(row=11, column=6, padx=1, pady=1, columnspan=1)
pantalla2.config(fg="black", justify="right")

pantalla3=Entry(miframe2, textvariable=L3IZZ4,width=4)
pantalla3.grid(row=11, column=7, padx=1, pady=1, columnspan=1)
pantalla3.config(fg="black", justify="right")





multvolZ2=StringVar(raiz)
MultimpZ2=StringVar(raiz)

multvolZ3=StringVar(raiz)
MultimpZ3=StringVar(raiz)

multvolZ4=StringVar(raiz)
MultimpZ4=StringVar(raiz)



opcion=OptionMenu(miframe2,multvolZ2,*multtensiones)
opcion.config(width=2)
opcion.grid(row=2, column=7)

opcion2=OptionMenu(miframe2,MultimpZ2,*multiimpedanciasLINEAS)
opcion2.config(width=2)
opcion2.grid(row=3, column=8)

opcion=OptionMenu(miframe2,multvolZ3,*multtensiones)
opcion.config(width=2)
opcion.grid(row=6, column=7)

opcion2=OptionMenu(miframe2,MultimpZ3,*multiimpedanciasLINEAS)
opcion2.config(width=2)
opcion2.grid(row=7, column=8)

opcion=OptionMenu(miframe2,multvolZ4,*multtensiones)
opcion.config(width=2)
opcion.grid(row=10, column=7)

opcion2=OptionMenu(miframe2,MultimpZ4,*multiimpedanciasLINEAS)
opcion2.config(width=2)
opcion2.grid(row=11, column=8)


#----------------------zona 5----------------------------------------------------------------------------------------------------------

zona1=Label(miframe2, text="ZONA 5:")
zona1.grid(row=0, column=10)
zona1.config(fg="black",font=("Verdana",8))


generador1=Label(miframe2, text="Generador 2:")
generador1.grid(row=1, column=9,padx=10)

tlabel=Label(miframe2, text=" S")
tlabel.grid(row=2, column=9)

tlabel=Label(miframe2, text=" V")
tlabel.grid(row=3, column=9)

tlabel=Label(miframe2, text=" Z")
tlabel.grid(row=4, column=9)

tlabel34=Label(miframe2, text=" +j")
tlabel34.place(x=832,y=115)
#tlabel.grid(row=5, column=3)

transformador2=Label(miframe2, text="Transformador 2:")
transformador2.grid(row=5, column=9,padx=10)

tlabel=Label(miframe2, text=" S")
tlabel.grid(row=6, column=9)

tlabel=Label(miframe2, text=" V")
tlabel.grid(row=7, column=9)

tlabel=Label(miframe2, text=" Z")
tlabel.grid(row=8, column=9)

tlabel34=Label(miframe2, text=" +j")
tlabel34.place(x=832,y=243)


transformador3=Label(miframe2, text="Transformador 4:")
transformador3.grid(row=9, column=9)

tlabel=Label(miframe2, text=" S")
tlabel.grid(row=10, column=9)

tlabel=Label(miframe2, text=" V")
tlabel.grid(row=11, column=9)

tlabel=Label(miframe2, text=" Z")
tlabel.grid(row=12, column=9)

tlabel34=Label(miframe2, text=" +j")
tlabel34.place(x=832,y=363)



G2SZ5=StringVar()
G2VZ5=StringVar()
G2RZZ5=StringVar()
G2IZZ5=StringVar()


T2SZ5=StringVar()
T2VZ5=StringVar()
T2RZZ5=StringVar()
T2IZZ5=StringVar()

T4SZ5=StringVar()
T4VZ5=StringVar()
T4RZZ5=StringVar()
T4IZZ5=StringVar()



pantalla3=Entry(miframe2, textvariable=G2SZ5,width=4)
pantalla3.grid(row=2, column=10, padx=1, pady=1, columnspan=1)
pantalla3.config(fg="black", justify="right")

pantalla4=Entry(miframe2, textvariable=G2VZ5,width=4)
pantalla4.grid(row=3, column=10, padx=1, pady=1, columnspan=1)
pantalla4.config(fg="black", justify="right")

pantalla5=Entry(miframe2, textvariable=G2RZZ5,width=4)
pantalla5.grid(row=4, column=10, padx=1, pady=1, columnspan=1)
pantalla5.config(fg="black", justify="right")

pantalla6=Entry(miframe2, textvariable=G2IZZ5,width=4)
pantalla6.grid(row=4, column=11, padx=20, pady=1, columnspan=1)
pantalla6.config(fg="black", justify="right")


pantalla3=Entry(miframe2, textvariable=T2SZ5,width=4)
pantalla3.grid(row=6, column=10, padx=1, pady=1, columnspan=1)
pantalla3.config(fg="black", justify="right")

pantalla4=Entry(miframe2, textvariable=T2VZ5,width=4)
pantalla4.grid(row=7, column=10, padx=1, pady=1, columnspan=1)
pantalla4.config(fg="black", justify="right")

pantalla5=Entry(miframe2, textvariable=T2RZZ5,width=4)
pantalla5.grid(row=8, column=10, padx=1, pady=1, columnspan=1)
pantalla5.config(fg="black", justify="right")

pantalla6=Entry(miframe2, textvariable=T2IZZ5,width=4)
pantalla6.grid(row=8, column=11, padx=1, pady=1, columnspan=1)
pantalla6.config(fg="black", justify="right")



pantalla3=Entry(miframe2, textvariable=T4SZ5,width=4)
pantalla3.grid(row=10, column=10, padx=1, pady=1, columnspan=1)
pantalla3.config(fg="black", justify="right")

pantalla4=Entry(miframe2, textvariable=T4VZ5,width=4)
pantalla4.grid(row=11, column=10, padx=1, pady=1, columnspan=1)
pantalla4.config(fg="black", justify="right")

pantalla5=Entry(miframe2, textvariable=T4RZZ5,width=4)
pantalla5.grid(row=12, column=10, padx=1, pady=1, columnspan=1)
pantalla5.config(fg="black", justify="right")

pantalla6=Entry(miframe2, textvariable=T4IZZ5,width=4)
pantalla6.grid(row=12, column=11, padx=1, pady=1, columnspan=1)
pantalla6.config(fg="black", justify="right")



multpotG2Z5=StringVar(raiz)
multpotT2Z5=StringVar(raiz)
multpotT4Z5=StringVar(raiz)
multvolG2Z5=StringVar(raiz)
multvolT2Z5=StringVar(raiz)
multvolT4Z5=StringVar(raiz)
MultimpG2Z5=StringVar(raiz)
MultimpT2Z5=StringVar(raiz)
MultimpT4Z5=StringVar(raiz)



opcion2=OptionMenu(miframe2,multpotG2Z5,*multpotencias)
opcion2.config(width=2)
opcion2.grid(row=2, column=11)

opcion2=OptionMenu(miframe2,multpotT2Z5,*multpotencias)
opcion2.config(width=2)
opcion2.grid(row=6, column=11)

opcion2=OptionMenu(miframe2,multpotT4Z5,*multpotencias)
opcion2.config(width=2)
opcion2.grid(row=10, column=11)



opcion3=OptionMenu(miframe2,multvolG2Z5,*multtensiones)
opcion3.config(width=2)
opcion3.grid(row=3, column=11)

opcion3=OptionMenu(miframe2,multvolT2Z5,*multtensiones)
opcion3.config(width=2)
opcion3.grid(row=7, column=11)

opcion3=OptionMenu(miframe2,multvolT4Z5,*multtensiones)
opcion3.config(width=2)
opcion3.grid(row=11, column=11)



opcion3=OptionMenu(miframe2,MultimpG2Z5,*multiimpedancias)
opcion3.config(width=2)
opcion3.grid(row=4, column=12, padx=1)

opcion3=OptionMenu(miframe2,MultimpT2Z5,*multiimpedancias)
opcion3.config(width=2)
opcion3.grid(row=8, column=12, padx=1)

opcion3=OptionMenu(miframe2,MultimpT4Z5,*multiimpedancias)
opcion3.config(width=2)
opcion3.grid(row=12, column=12, padx=1)



#----------------------zona 6--------------------------------------------------------------------------------------------------------------------------

zona1=Label(miframe2, text="ZONA 6:")
zona1.grid(row=0, column=14)
zona1.config(fg="black",font=("Verdana",8))

transformador2=Label(miframe2, text="Transformador 5:")
transformador2.grid(row=1, column=13,padx=1)

tlabel=Label(miframe2, text=" S")
tlabel.grid(row=2, column=13)

tlabel=Label(miframe2, text=" V Prim")
tlabel.grid(row=3, column=13)

tlabel=Label(miframe2, text=" V sec")
tlabel.grid(row=4, column=13)

tlabel=Label(miframe2, text=" Z")
tlabel.grid(row=5, column=13)

tlabel34=Label(miframe2, text="   +j")
tlabel34.place(x=1195,y=148)

transformador2=Label(miframe2, text="Motor 1:")
transformador2.grid(row=6, column=13,padx=1)

tlabel=Label(miframe2, text=" S")
tlabel.grid(row=7, column=13)

tlabel=Label(miframe2, text=" V")
tlabel.grid(row=8, column=13)

tlabel=Label(miframe2, text=" Z")
tlabel.grid(row=9, column=13)

tlabel34=Label(miframe2, text="   +j")
tlabel34.place(x=1195,y=272)


T5SZ6=StringVar()
T5VZ6=StringVar()
T5V2Z6=StringVar()####nueva
T5RZZ6=StringVar()
T5IZZ6=StringVar()

M1SZ6=StringVar()
M1VZ6=StringVar()
M1RZZ6=StringVar()
M1IZZ6=StringVar()

pantalla3=Entry(miframe2, textvariable=T5SZ6,width=4)
pantalla3.grid(row=2, column=14, padx=1, pady=1, columnspan=1)
pantalla3.config(fg="black", justify="right")

pantalla4=Entry(miframe2, textvariable=T5VZ6,width=4)
pantalla4.grid(row=3, column=14, padx=1, pady=1, columnspan=1)
pantalla4.config(fg="black", justify="right")

pantalla4=Entry(miframe2, textvariable=T5V2Z6,width=4)
pantalla4.grid(row=4, column=14, padx=1, pady=1, columnspan=1)
pantalla4.config(fg="black", justify="right")

pantalla5=Entry(miframe2, textvariable=T5RZZ6,width=4)
pantalla5.grid(row=5, column=14, padx=1, pady=1, columnspan=1)
pantalla5.config(fg="black", justify="right")

pantalla6=Entry(miframe2, textvariable=T5IZZ6,width=4)
pantalla6.grid(row=5, column=15, padx=1, pady=1, columnspan=1)
pantalla6.config(fg="black", justify="right")



pantalla3=Entry(miframe2, textvariable=M1SZ6,width=4)
pantalla3.grid(row=7, column=14, padx=1, pady=1, columnspan=1)
pantalla3.config(fg="black", justify="right")

pantalla4=Entry(miframe2, textvariable=M1VZ6,width=4)
pantalla4.grid(row=8, column=14, padx=1, pady=1, columnspan=1)
pantalla4.config(fg="black", justify="right")

pantalla5=Entry(miframe2, textvariable=M1RZZ6,width=4)
pantalla5.grid(row=9, column=14, padx=1, pady=1, columnspan=1)
pantalla5.config(fg="black", justify="right")

pantalla6=Entry(miframe2, textvariable=M1IZZ6,width=4)
pantalla6.grid(row=9, column=15, padx=20, pady=1, columnspan=1)
pantalla6.config(fg="black", justify="right")


multpotM1Z6=StringVar()
multpotT5Z6=StringVar()
multvolM1Z6=StringVar()
multvolT5Z6=StringVar()
multvol2T5Z6=StringVar()###nueva
MultimpM1Z6=StringVar()
MultimpT5Z6=StringVar()


opcion2=OptionMenu(miframe2,multpotM1Z6,*multpotencias)
opcion2.config(width=2)
opcion2.grid(row=7, column=15)

opcion2=OptionMenu(miframe2,multpotT5Z6,*multpotencias)
opcion2.config(width=2)
opcion2.grid(row=2, column=15)


opcion3=OptionMenu(miframe2,multvolM1Z6,*multtensiones)
opcion3.config(width=2)
opcion3.grid(row=8, column=15)

opcion3=OptionMenu(miframe2,multvolT5Z6,*multtensiones)
opcion3.config(width=2)
opcion3.grid(row=3, column=15)

opcion3=OptionMenu(miframe2,multvol2T5Z6,*multtensiones)
opcion3.config(width=2)
opcion3.grid(row=4, column=15)



opcion3=OptionMenu(miframe2,MultimpM1Z6,*multiimpedancias)
opcion3.config(width=2)
opcion3.grid(row=9, column=16)

opcion3=OptionMenu(miframe2,MultimpT5Z6,*multiimpedancias)
opcion3.config(width=2)
opcion3.grid(row=5, column=16)




#---------------resultados-----------------------------------------------------------------------------------------------------------------------
tensionlabel=Label(miframe3, text="Resultados en PU:")
tensionlabel.grid(row=0, column=4)

resultimpbasez1=StringVar()
resultimpbasez2=StringVar()
resultimpbasez3=StringVar()
resultimpbasez4=StringVar()
resultimpbasez5=StringVar()
resultimpbasez6=StringVar()


resulimpT1=StringVar()
resulimpT2=StringVar()
resulimpT3=StringVar()
resulimpT4=StringVar()
resulimpT5=StringVar()

resulimpG1=StringVar()
resulimpG2=StringVar()
resulimpM1=StringVar()

resulimpL1=StringVar()
resulimpL2=StringVar()
resulimpL3=StringVar()

resulttensionbasez2=StringVar()
resulttensionbasez3=StringVar()
resulttensionbasez4=StringVar()
resulttensionbasez5=StringVar()
resulttensionbasez6=StringVar()

#------------------------------iMPEDANCIAS BASE ---------------------------------

tensionlabel=Label(miframe3, text="Impedancias base")
tensionlabel.grid(row=1, column=2)

tensionlabel=Label(miframe3, text="Z1:")
tensionlabel.grid(row=2, column=1, padx=10)

resultadoz6=Entry(miframe3, textvariable=resultimpbasez1,width=4)
resultadoz6.grid(row=2, column=2, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="Z2:")
tensionlabel.grid(row=3, column=1)

resultadoz6=Entry(miframe3, textvariable=resultimpbasez2,width=4)
resultadoz6.grid(row=3, column=2, padx=5, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="Z3:")
tensionlabel.grid(row=4, column=1)

resultadoz6=Entry(miframe3, textvariable=resultimpbasez3,width=4)
resultadoz6.grid(row=4, column=2, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="Z4:")
tensionlabel.grid(row=5, column=1)

resultadoz6=Entry(miframe3, textvariable=resultimpbasez4,width=4)
resultadoz6.grid(row=5, column=2, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="Z5:")
tensionlabel.grid(row=6, column=1)

resultadoz6=Entry(miframe3, textvariable=resultimpbasez5,width=4)
resultadoz6.grid(row=6, column=2, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="Z6:")
tensionlabel.grid(row=7, column=1)

resultadoz6=Entry(miframe3, textvariable=resultimpbasez6,width=4)
resultadoz6.grid(row=7, column=2, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

#------------------------------
tensionlabel=Label(miframe3, text="Tensiones base ")
tensionlabel.grid(row=1, column=4)


tensionlabel=Label(miframe3, text="Z2:")
tensionlabel.grid(row=2, column=3)

resultadoz6=Entry(miframe3, textvariable=resulttensionbasez2,width=4)
resultadoz6.grid(row=2, column=4, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="Z3:")
tensionlabel.grid(row=3, column=3)

resultadoz6=Entry(miframe3, textvariable=resulttensionbasez3,width=4)
resultadoz6.grid(row=3, column=4, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="Z4:")
tensionlabel.grid(row=4, column=3)

resultadoz6=Entry(miframe3, textvariable=resulttensionbasez4,width=4)
resultadoz6.grid(row=4, column=4, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="Z5:")
tensionlabel.grid(row=5, column=3)

resultadoz6=Entry(miframe3, textvariable=resulttensionbasez5,width=4)
resultadoz6.grid(row=5, column=4, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="Z6:")
tensionlabel.grid(row=6, column=3)

resultadoz6=Entry(miframe3, textvariable=resulttensionbasez6,width=4)
resultadoz6.grid(row=6, column=4, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

#----------------------- Z  TRAFOS-------------------- 

tensionlabel=Label(miframe3, text="Impedacia Trafos")
tensionlabel.grid(row=1, column=6, padx=1)

tensionlabel=Label(miframe3, text="     T1:")
tensionlabel.grid(row=2, column=5, padx=1)

resultadoz6=Entry(miframe3, textvariable=resulimpT1,width=4)
resultadoz6.grid(row=2, column=6, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="     T2:")
tensionlabel.grid(row=3, column=5)

resultadoz6=Entry(miframe3, textvariable=resulimpT2,width=4)
resultadoz6.grid(row=3, column=6, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="     T3:")
tensionlabel.grid(row=4, column=5)

resultadoz6=Entry(miframe3, textvariable=resulimpT3,width=4)
resultadoz6.grid(row=4, column=6, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="     T4:")
tensionlabel.grid(row=5, column=5)

resultadoz6=Entry(miframe3, textvariable=resulimpT4,width=4)
resultadoz6.grid(row=5, column=6, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="     T5:")
tensionlabel.grid(row=6, column=5)

resultadoz6=Entry(miframe3, textvariable=resulimpT5,width=4)
resultadoz6.grid(row=6, column=6, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

#----------------------- ZE
tensionlabel=Label(miframe3, text=" Impedancia Elementos")
tensionlabel.grid(row=7, column=4)


tensionlabel=Label(miframe3, text="G1:")
tensionlabel.grid(row=8, column=3)

resultadoz6=Entry(miframe3, textvariable=resulimpG1,width=4)
resultadoz6.grid(row=8, column=4, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="G2:")
tensionlabel.grid(row=9, column=3)

resultadoz6=Entry(miframe3, textvariable=resulimpG2,width=4)
resultadoz6.grid(row=9, column=4, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text=" M ")
tensionlabel.grid(row=10, column=3)

resultadoz6=Entry(miframe3, textvariable=resulimpM1,width=4)
resultadoz6.grid(row=10, column=4, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")


#----------------------- ZL

tensionlabel=Label(miframe3, text=" Impedancias Lineas")
tensionlabel.grid(row=7, column=6, padx=1)


tensionlabel=Label(miframe3, text="     L1:")
tensionlabel.grid(row=8, column=5, padx=1)

resultadoz6=Entry(miframe3, textvariable=resulimpL1,width=4)
resultadoz6.grid(row=8, column=6, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")

tensionlabel=Label(miframe3, text="     L2:")
tensionlabel.grid(row=9, column=5)

resultadoz6=Entry(miframe3, textvariable=resulimpL2,width=4)
resultadoz6.grid(row=9, column=6, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")


tensionlabel=Label(miframe3, text="     L3:")
tensionlabel.grid(row=10, column=5)

resultadoz6=Entry(miframe3, textvariable=resulimpL3,width=4)
resultadoz6.grid(row=10, column=6, padx=1, pady=1, columnspan=1)
resultadoz6.config(fg="black", justify="right")





#---------Cálculo


botonresultado=Button(miframe2, text="Calcular", width=6, command=lambda:zetaBase())
botonresultado.grid(row=13, column=16)






raiz.mainloop()
 
