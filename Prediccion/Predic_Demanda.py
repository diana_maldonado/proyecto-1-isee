#!/usr/bin/env python
# coding: utf-8

# 
# ## Predicción

# In[9]:


import pandas as pd
import numpy as np

data = pd.read_csv("Datos_Demanda.csv", index_col = 0, parse_dates = True)
data.columns = [ "T_Dia", "Potencia"]
data


# In[10]:


data.corr()


# In[11]:


data1 = data.loc[:,['Potencia']]
data1


# In[12]:


data2 = data.loc[:,['T_Dia']]
data2


# In[13]:


data1.plot(figsize=(16,7), xlabel='Fecha', ylabel='Potencia (W)', grid=True, title='Datos de Potencia')
print()


# In[14]:


data2.plot(figsize=(16,7), xlabel='Fecha', ylabel='Tipo de dia', grid=True, title='Datos de tipos de dia')
print()


# In[15]:


data1.describe()


# In[16]:


data2.describe()


# In[17]:


n_train1 = int(0.9799*len(data1))    #0.8439 Diciembre  #
n_train2 = int(0.9799*len(data2))
train1, test1 = data1.iloc[0:n_train1,:], data1.iloc[n_train1:len(data1),:]
train2, test2 = data2.iloc[0:n_train2,:], data2.iloc[n_train2:len(data2),:]

print('cantidad de datos train:', len(train1))
print('cantidad de datos test:', len(test1))
print('cantidad de datos train:', len(train2))
print('cantidad de datos test:', len(test2))


# In[18]:


test1


# In[19]:


train1


# In[20]:


train2


# In[21]:


test1


# In[22]:


from sklearn.preprocessing import MinMaxScaler

transformer1 = MinMaxScaler(feature_range=(0,1))
transformer1 = transformer1.fit(train1.to_numpy())

transformer2 = MinMaxScaler(feature_range=(0,1))
transformer2 = transformer2.fit(train2.to_numpy())

train_escaled1 = train1.copy()
train_escaled1.loc[:,:] = transformer1.transform(train1.to_numpy())


train_escaled2 = train2.copy()
train_escaled2.loc[:,:] = transformer2.transform(train2.to_numpy())

train_escaled1


# In[23]:


train_escaled2


# In[24]:


def create_dataset(X,Y,time_steps=1):
    Xs, Ys=[],[]
    for i in range(0,len(X)-(time_steps+6)):
        Xs.append(X.iloc[i:(i+time_steps)].to_numpy())
        Ys.append(Y.iloc[i+time_steps:i+time_steps+6].values.flatten())
    return np.array(Xs), np.array(Ys)


# In[25]:


X_train1, Y_train1 = create_dataset(train_escaled1,train_escaled1,time_steps=6)
X_train2, Y_train2 = create_dataset(train_escaled2,train_escaled2,time_steps=6)
print("Dimensiones de X_train1: ", X_train1.shape)
print("X_train1: \n", X_train1, "\n")
print("Dimensiones de Y_train1: ", Y_train1.shape)
print("Y_train1: \n", Y_train1, "\n")

print("Dimensiones de X_train2: ", X_train2.shape)
print("X_train2: \n", X_train2, "\n")
print("Dimensiones de Y_train2: ", Y_train2.shape)
print("Y_train2: \n", Y_train2, "\n")


# In[26]:


from keras.models import Sequential
from keras.layers import Input
from keras.layers import SimpleRNN
from keras.layers import Dense

red = Sequential()
red.add(Input(shape=(6,1)))
red.add(SimpleRNN(100))
red.add(Dense(6))

red.compile(loss='mean_squared_error', optimizer='adam')

red.summary()


# ## Entrenamiento de la red

# In[27]:


history = red.fit(X_train1,Y_train1,epochs=200,validation_split=0.1, shuffle=False)
#history = red.fit(X_train1,Y_train1,epochs=25,validation_split=0.1, shuffle=False)


# In[28]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

plt.figure(figsize = (15,7))
plt.title('Progreso del entrenamiento')
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.legend(['loss', 'val_loss'])
plt.grid()


# In[29]:


df1=pd.DataFrame()
df1['loss'] = history.history['loss']
df1['val_loss'] = history.history['val_loss']
ruta1 = "Ent_Demanda.csv"
df1.to_csv(ruta1)


# ## Preparacion del set de datos de test

# In[30]:


test_escaled1 = test1.copy()
test_escaled1.loc[:,:] =transformer1.transform(test1.to_numpy())

X_test, Y_test = create_dataset(test_escaled1, test_escaled1, time_steps=6)


# ## Prediccion

# In[31]:


Y_predict_inv = red.predict(X_test)
Y_pred = transformer1.inverse_transform(Y_predict_inv)
Y_test_ = transformer1.inverse_transform(Y_test)

print(Y_pred.shape)


# In[32]:


data_interes = np.arange(0,Y_pred.shape[0],6)
predicha= Y_pred[data_interes,:].flatten()
real = Y_test_[data_interes,:].flatten()
fechas = test1.index[0:len(predicha)]
fechas.columns = ['Timestamp']
df=pd.DataFrame(index=fechas)
#df['Fechas'] = fechas
df['Real'] = real
df['Predicho'] = predicha
df = df.rename_axis('Timestamp').reset_index()
df.set_index('Timestamp',inplace=True)
ruta = "Pre_Demanda.csv"
df.to_csv(ruta)

dias_a_graficar = 10

plt.figure(figsize=(17,7))
fechas = fechas[0:24*dias_a_graficar]
plt.plot(fechas,predicha[0:24*dias_a_graficar])
plt.plot(fechas,real[0:24*dias_a_graficar])
plt.title('Resultados')
plt.xlabel("Fecha")
plt.ylabel("Potencia (W)")
plt.legend(['Prediccion', 'Real'])
plt.grid()


# In[33]:


df


# In[34]:


from sklearn.metrics import mean_squared_error
print("MSE: ", mean_squared_error(predicha,real))


# In[35]:


mse = (np.square(real - predicha)).mean()
mse


# In[36]:


np.square(real.mean() - predicha.mean())


# In[37]:


predicha.mean()


# In[38]:


real.mean()


# In[39]:


real


# In[ ]:




