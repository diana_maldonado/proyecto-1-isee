import streamlit as st
import pandas as pd
import numpy as np
import base64
from PIL import Image

LOGO_IMAGE = "App.png"

st.markdown(
    """
    <style>
    .container {
        display: flex;
    }
    .logo-text {
        font-weight:700 !important;
        font-size:70px !important;
        color: #5eb880 !important;
        padding-top: 30px !important;

    }
    .logo-img {
        float:left;
    }
    </style>
    """,
    unsafe_allow_html=True
)
st.markdown(
    f"""
    <div class="container">
        <img class="logo-img" width="120" height="150" src="data:image/png;base64,{base64.b64encode(open(LOGO_IMAGE, "rb").read()).decode()}">
        <p class="logo-text">Power Predictor</p>
    </div>
    """,
    unsafe_allow_html=True
)


tipo = st.sidebar.selectbox(
    "Demo:",
    ['','ON','OFF'],format_func=lambda x: 'Seleccione una opción' if x == '' else x)   

if tipo == "ON":

    ##leer datos 
    #perfiles = pd.read_excel('data.xlsx', index_col =0)
        Data_Solar=pd.read_csv('Datos_Solar.csv', index_col =0)
        Data_Eolica=pd.read_csv('Datos_Eolica.csv', index_col =0)
        Data_Demanda=pd.read_csv('Datos_Demanda.csv', index_col =0)

    ## Entrenamiento
        E_Solar = pd.read_csv('Ent_Solar.csv', index_col =0)
        E_Eolica = pd.read_csv('Ent_Eolica.csv', index_col =0)
        E_Demanda = pd.read_csv('Ent_Demanda.csv', index_col =0)

        P_Solar = pd.read_csv('Pre_Solar.csv', index_col =0)
        P_Demanda = pd.read_csv('Pre_Demanda.csv', index_col =0)
        P_Eolica = pd.read_csv('Pre_Eolica.csv', index_col =0)
        variables = ['','Demanda Energética', 'Generación Solar', 'Generación Eólica']         

        opcion = st.sidebar.selectbox("Elije la predicción:", variables, format_func=lambda x: 'Seleccione una opción' if x == '' else x)
        datos =[]
        Entrenamiento = []
        Prediccion = []

        flagOption = 0

        if opcion =="Generación Solar":
            st.subheader(":ballot_box_with_check: Se ha seleccionado: "+ opcion)
            st.success(":page_facing_up: Datos de entrada:")
            datos=Data_Solar
            Entrenamiento = E_Solar
            Prediccion = P_Solar
            dias_a_graficar2 =1
            flagOption = 1
            st.dataframe(datos)
        elif opcion == "Demanda Energética":
            st.subheader(":ballot_box_with_check: Se ha seleccionado: "+ opcion)
            st.success(":page_facing_up: Datos de entrada:  ")
            datos = Data_Demanda
            Entrenamiento = E_Demanda
            Prediccion = P_Demanda
            dias_a_graficar2 =7
            flagOption = 1
            st.dataframe(datos)
        elif opcion == "Generación Eólica":
            st.subheader(":ballot_box_with_check: Se ha seleccionado: "+ opcion)
            st.success(":page_facing_up: Datos de entrada:  ")
            datos= Data_Eolica
            Entrenamiento = E_Eolica
            Prediccion = P_Eolica
            dias_a_graficar2 =1
            flagOption = 1
            st.dataframe(datos)
        else:
            st.header(" ")
            st.header("Instrucciones:")
            st.subheader(":point_left: En la barra Elije la predicción:")
            col1, col2 = st.beta_columns([4,6])

            with col1:
                st.write(" ")
            with col2:
                st.write(":ballot_box_with_check: Seleccione una predicción")
        
            ##Escojer que quiero ver de las variables de entrada
                
        if flagOption == 1:
            variables2 = datos.columns.values
            variable3 = "Selecciona una opción" + datos.columns.values
            opcion2 = st.sidebar.selectbox("Elije la variable", variables2)
            st.subheader(":ballot_box_with_check: Variable seeleccionada: "+ opcion2)

            ##grafico de la variable 
            st.info(":chart_with_downwards_trend: Gráfico de datos de entrada")
            dias_a_graficar = st.slider("Días a graficar", min_value=1, max_value=10, value=1)
            st.line_chart(datos[opcion2][0:24*dias_a_graficar])

            ## Grafica del entrenamiento 
            st.warning(":woman-running: Gráfica del entrenamiento")
            st.line_chart(Entrenamiento)

            st.error(":chart_with_upwards_trend: :chart_with_upwards_trend: Gráfica de la comparación")
            ## Grafico de la prediccion 
            st.line_chart(Prediccion[['Real','Predicho']][0:24*dias_a_graficar2])
            #st.line_chart(Prediccion)

elif tipo == "OFF":

    variable = 0

    file = st.sidebar.file_uploader("Elige un archivo CSV", type="csv")
    if file is not None:
        dataFile = pd.read_csv(file, index_col = 0, parse_dates = True)
        variable =1
    else:
        st.text(" ")
        st.header("Instrucciones:")
        st.subheader(":point_left: :file_folder: Cargue un archivo csv en Browse files")
    
    if variable == 1:
        epocas = st.sidebar.slider("Épocas", min_value=1, max_value=20, value=0)
        if file.name == "Datos_Demanda.csv" and epocas != 0:
            st.subheader(":ballot_box_with_check: El archivo csv corresponde a una predicción de Demanda Energética ")
            if epocas == 0:
                st.error("Pon un número de épocas")
            else:
                data = dataFile
                st.success(":page_facing_up: Datos de entrada:")
                st.dataframe(data)
                data.columns = [ "Tipo de día", "Potencia"]
                data1 = data.loc[:,['Potencia']]
                data2 = data.loc[:,['Tipo de día']]
                st.info(":chart_with_upwards_trend: Gráfico de datos de entrada")
                st.line_chart(data1)
                n_train1 = int(0.9799*len(data1))    #0.8439 Diciembre  #
                n_train2 = int(0.9799*len(data2))
                train1, test1 = data1.iloc[0:n_train1,:], data1.iloc[n_train1:len(data1),:]
                train2, test2 = data2.iloc[0:n_train2,:], data2.iloc[n_train2:len(data2),:]
                from sklearn.preprocessing import MinMaxScaler

                transformer1 = MinMaxScaler(feature_range=(0,1))
                transformer1 = transformer1.fit(train1.to_numpy())

                transformer2 = MinMaxScaler(feature_range=(0,1))
                transformer2 = transformer2.fit(train2.to_numpy())

                train_escaled1 = train1.copy()
                train_escaled1.loc[:,:] = transformer1.transform(train1.to_numpy())
                train_escaled2 = train2.copy()
                train_escaled2.loc[:,:] = transformer2.transform(train2.to_numpy())

                def create_dataset(X,Y,time_steps=1):
                    Xs, Ys=[],[]
                    for i in range(0,len(X)-(time_steps+6)):
                        Xs.append(X.iloc[i:(i+time_steps)].to_numpy())
                        Ys.append(Y.iloc[i+time_steps:i+time_steps+6].values.flatten())
                    return np.array(Xs), np.array(Ys)

                X_train1, Y_train1 = create_dataset(train_escaled1,train_escaled1,time_steps=6)
                X_train2, Y_train2 = create_dataset(train_escaled2,train_escaled2,time_steps=6)
                from keras.models import Sequential
                from keras.layers import Input
                from keras.layers import SimpleRNN
                from keras.layers import Dense

                red = Sequential()
                red.add(Input(shape=(6,1)))
                red.add(SimpleRNN(100))
                red.add(Dense(6))

                red.compile(loss='mean_squared_error', optimizer='adam')
                history = red.fit(X_train1,Y_train1,epochs=int(epocas),validation_split=0.1, shuffle=False)
                df1=pd.DataFrame()
                df1['loss'] = history.history['loss']
                df1['val_loss'] = history.history['val_loss']
                ruta1 = "Ent_Demanda_app.csv"
                df1.to_csv(ruta1)
                st.warning(":woman-running: Gráfica del entrenamiento")
                st.line_chart(df1)
                test_escaled1 = test1.copy()
                test_escaled1.loc[:,:] =transformer1.transform(test1.to_numpy())
                X_test, Y_test = create_dataset(test_escaled1, test_escaled1, time_steps=6)

                Y_predict_inv = red.predict(X_test)
                Y_pred = transformer1.inverse_transform(Y_predict_inv)
                Y_test_ = transformer1.inverse_transform(Y_test)

                data_interes = np.arange(0,Y_pred.shape[0],6)
                predicha= Y_pred[data_interes,:].flatten()
                real = Y_test_[data_interes,:].flatten()
                fechas = test1.index[0:len(predicha)]
                fechas.columns = ['Timestamp']
                df=pd.DataFrame(index=fechas)
                #df['Fechas'] = fechas
                df['Real'] = real
                df['Predicho'] = predicha
                df = df.rename_axis('Timestamp').reset_index()
                df.set_index('Timestamp',inplace=True)
                ruta = "Pre_Demanda_app.csv"
                df.to_csv(ruta)
                st.error(":chart_with_upwards_trend: :chart_with_upwards_trend: Gráfica de la comparación")
                st.line_chart(df[0:24*7])

        elif file.name == "Datos_Solar.csv" and epocas != 0:
            st.subheader(":ballot_box_with_check: El archivo csv corresponde a una predicción de Generación Solar")
            import tensorflow as tf
            from keras.layers import Dense, Activation, BatchNormalization, Dropout
            from keras import regularizers
            from keras.optimizers import RMSprop, Adam, SGD
            import datetime
            import matplotlib.pyplot as plt
            import seaborn as sns
            import matplotlib.pyplot as plt
            dts = dataFile
            st.success(":page_facing_up: Datos de entrada:")
            st.dataframe(dts)
            dts.columns = ["T","VV","I", "Potencia"]
            X = dts.iloc[:, :-1].values
            y = dts.iloc[:, -1].values
            y = np.reshape(y, (-1,1))
            st.info(":chart_with_upwards_trend: Gráfico de datos de entrada")
            st.line_chart(y)
            from sklearn.model_selection import train_test_split
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.078, shuffle=False)
            n_train = int(0.922*len(dts))

            train1, test1 = dts.iloc[0:n_train,:-1], dts.iloc[n_train:len(dts),:-1]
            data2 = dts.loc[:,['Potencia']]
            n_train2 = int(0.922*len(data2))
            train2, test2= data2.iloc[0:n_train2,:], data2.iloc[n_train2:len(data2),:]

            from sklearn.preprocessing import StandardScaler
            # input scaling
            sc_X = StandardScaler()
            X_train = sc_X.fit_transform(X_train)
            X_test = sc_X.transform(X_test)

            # outcome scaling:
            sc_y = StandardScaler()
            y_train = sc_y.fit_transform(y_train)    
            y_test = sc_y.transform(y_test)

            def create_spfnet(n_layers, n_activation, kernels):
                model = tf.keras.models.Sequential()
                for i, nodes in enumerate(n_layers):
                    if i==0:
                        model.add(Dense(nodes, kernel_initializer=kernels, activation=n_activation, input_dim=X_train.shape[1]))
                    else:
                        model.add(Dense(nodes, activation=n_activation, kernel_initializer=kernels))
                
                model.add(Dense(1))
                model.compile(loss='mse', 
                                optimizer='adam',
                                metrics=[tf.keras.metrics.RootMeanSquaredError()])
                return model

            spfnet = create_spfnet([32, 64], 'relu', 'normal')
            hist = spfnet.fit(X_train, y_train, batch_size=32, validation_data=(X_test, y_test),epochs=int(epocas), verbose=2, shuffle=False)
            df1=pd.DataFrame()
            df1['loss'] = hist.history['loss']
            df1['val_loss'] = hist.history['val_loss']
            ruta1 = "Ent_Solar_app.csv"
            df1.to_csv(ruta1)
            spfnet.evaluate(X_train, y_train)
            st.warning(":woman-running: Gráfica del entrenamiento")
            st.line_chart(df1)
            from sklearn.metrics import mean_squared_error

            y_pred = spfnet.predict(X_test) # get model predictions (scaled inputs here)
            y_pred_orig = sc_y.inverse_transform(y_pred) # unscale the predictions
            y_test_orig = sc_y.inverse_transform(y_test) # unscale the true test outcomes

            RMSE_orig = mean_squared_error(y_pred_orig, y_test_orig, squared=False)
            train_pred = spfnet.predict(X_train) # get model predictions (scaled inputs here)
            train_pred_orig = sc_y.inverse_transform(train_pred) # unscale the predictions
            y_train_orig = sc_y.inverse_transform(y_train) # unscale the true train outcomes
            results = np.concatenate((y_test_orig, y_pred_orig), 1)
            results = pd.DataFrame(data=results,index=test2.index[:])
            results.columns = ['Produced', 'Predicted']
            pd.options.display.float_format = "{:,.2f}".format
            predicho = results.loc[:,['Predicted']]
            real = results.loc[:,['Produced']]
            fechas = test2.index[:]
            df=pd.DataFrame(index=fechas)
            df['Real'] = real['Produced'].values
            df['Predicho'] = predicho['Predicted'].values
            dias_a_graficar = 1
            ruta = "Pre_Solar_app.csv"
            df.to_csv(ruta)
            st.error(":chart_with_upwards_trend: :chart_with_upwards_trend: Gráfica de la comparación")
            st.line_chart(df[0:24*1])

        elif file.name == "Datos_Eolica.csv" and epocas != 0:
            st.subheader(":ballot_box_with_check: El archivo csv corresponde a una predicción de Generación Eólica   ")
            import tensorflow as tf
            from keras.layers import Dense, Activation, BatchNormalization, Dropout
            from keras import regularizers
            from keras.optimizers import RMSprop, Adam, SGD
            import datetime
            import matplotlib.pyplot as plt
            import seaborn as sns
            import matplotlib.pyplot as plt
            dts = dataFile
            st.success(":page_facing_up: Datos de entrada:")
            st.dataframe(dts)
            dts.columns = ["T","VV", "Potencia"]

            X = dts.iloc[:, :-1].values
            y = dts.iloc[:, -1].values
            print(X.shape, y.shape)
            y = np.reshape(y, (-1,1))
            st.info(":chart_with_upwards_trend: Gráfico de datos de entrada")
            st.line_chart(y)
            from sklearn.model_selection import train_test_split
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.078, shuffle=False)
            n_train = int(0.922*len(dts))

            train1, test1 = dts.iloc[0:n_train,:-1], dts.iloc[n_train:len(dts),:-1]
            data3 = dts.loc[:,['Potencia']]
            n_train2 = int(0.922*len(data3))
            train2, test2= data3.iloc[0:n_train2,:], data3.iloc[n_train2:len(data3),:]
            from sklearn.preprocessing import StandardScaler
            # input scaling
            sc_X = StandardScaler()
            X_train = sc_X.fit_transform(X_train)
            X_test = sc_X.transform(X_test)

            # outcome scaling:
            sc_y = StandardScaler()
            y_train = sc_y.fit_transform(y_train)    
            y_test = sc_y.transform(y_test)
            def create_spfnet(n_layers, n_activation, kernels):
                model = tf.keras.models.Sequential()
                for i, nodes in enumerate(n_layers):
                    if i==0:
                        model.add(Dense(nodes, kernel_initializer=kernels, activation=n_activation, input_dim=X_train.shape[1]))
                #model.add(Dropout(0.3))
                    else:
                        model.add(Dense(nodes, activation=n_activation, kernel_initializer=kernels))
                #model.add(Dropout(0.3))
                model.add(Dense(1))
                model.compile(loss='mse', 
                            optimizer='adam',
                            metrics=[tf.keras.metrics.RootMeanSquaredError()])
                return model
            spfnet = create_spfnet([32, 64], 'relu', 'normal')
            hist = spfnet.fit(X_train, y_train, batch_size=32, validation_data=(X_test, y_test),epochs=int(epocas), verbose=2, shuffle=False)
            df2=pd.DataFrame()
            df2['loss'] = hist.history['loss']
            df2['val_loss'] = hist.history['val_loss']
            ruta1 = "Ent_Eolica_app.csv"
            df2.to_csv(ruta1)
            st.warning(":woman-running: Gráfica del entrenamiento")
            st.line_chart(df2)
            spfnet.evaluate(X_train, y_train)
            from sklearn.metrics import mean_squared_error

            y_pred = spfnet.predict(X_test) # get model predictions (scaled inputs here)
            y_pred_orig = sc_y.inverse_transform(y_pred) # unscale the predictions
            y_test_orig = sc_y.inverse_transform(y_test) # unscale the true test outcomes

            RMSE_orig = mean_squared_error(y_pred_orig, y_test_orig, squared=False)

            train_pred = spfnet.predict(X_train) # get model predictions (scaled inputs here)
            train_pred_orig = sc_y.inverse_transform(train_pred) # unscale the predictions
            y_train_orig = sc_y.inverse_transform(y_train) # unscale the true train outcomes
            results = np.concatenate((y_test_orig, y_pred_orig), 1)
            results = pd.DataFrame(data=results,index=test2.index[:])
            results.columns = ['Produced', 'Predicted']
            #results = results.sort_values(by=['Real Solar Power Produced'])
            pd.options.display.float_format = "{:,.2f}".format
            predicho = results.loc[:,['Predicted']]
            real = results.loc[:,['Produced']]
            fechas = test2.index[:]
            df3=pd.DataFrame(index=fechas)
            #df['Fechas'] = fechas
            df3['Real'] = real['Produced'].values
            df3['Predicho'] = predicho['Predicted'].values
            dias_a_graficar = 10
            ruta = "Pre_Eolica_app.csv"
            df3.to_csv(ruta)
            st.error(":chart_with_upwards_trend: :chart_with_upwards_trend: Gráfica de la comparación")
            #st.line_chart(df3[0:24*1])

        else:
            st.header("Instrucciones:")
            st.subheader(":point_left: :1234: Seleccione la cantidad de épocas ") 

else:
    st.title('Bienvenido :wave:')
    st.header("Instrucciones:")
    st.subheader(":point_left: En la barra Demo:")
    col1, col2 = st.beta_columns([1,6])

    with col1:
        st.write(" ")
    with col2:
        st.write("     :ballot_box_with_check:    Seleccione ON para ver el modo demostración.")
        st.write("     :ballot_box_with_check:   Seleccione OFF para realizar predicciones a partir de un archivo csv ")
