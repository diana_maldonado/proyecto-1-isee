#!/usr/bin/env python
# coding: utf-8

# In[180]:


import pandas as pd
import numpy as np
import tensorflow as tf
from keras.layers import Dense, Activation, BatchNormalization, Dropout
from keras import regularizers
from keras.optimizers import RMSprop, Adam, SGD
import datetime
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.pyplot as plt


# In[181]:


dts = pd.read_csv("Datos_Solar.csv", index_col = 0, parse_dates = True)
dts.columns = ["T","VV","I", "Potencia"]

#dts = dts.loc[:,["VV","T", "Potencia"]]
dts


# In[182]:


X = dts.iloc[:, :-1].values
y = dts.iloc[:, -1].values
print(X.shape, y.shape)
y = np.reshape(y, (-1,1))
y.shape


# In[183]:


X


# In[184]:


y


# In[221]:


y.plot(figsize=(16,7), xlabel='Fecha', ylabel='Potencia (W)', grid=True, title='Datos de Potencia')
print()
plt.plot(y)
plt.show()


# In[186]:


X.plot(figsize=(16,7), xlabel='Fecha', ylabel='Tipo de dia', grid=True, title='Datos de tipos de dia')
print()
plt.plot(X)
plt.show()


# In[187]:


from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.078, shuffle=False)
print("Train Shape: {} {} \nTest Shape: {} {}".format(X_train.shape, y_train.shape, X_test.shape, y_test.shape))


# In[ ]:





# In[188]:


X_train


# In[189]:


n_train = int(0.922*len(dts))

train1, test1 = dts.iloc[0:n_train,:-1], dts.iloc[n_train:len(dts),:-1]


# In[190]:


data2 = dts.loc[:,['Potencia']]
n_train2 = int(0.922*len(data2))
train2, test2= data2.iloc[0:n_train2,:], data2.iloc[n_train2:len(data2),:]
train2


# In[191]:


borrar=test2.iloc[0:,:-3]
borrar


# In[192]:


test2


# In[193]:


train1


# In[194]:


test1


# In[195]:


from sklearn.preprocessing import StandardScaler
# input scaling
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

# outcome scaling:
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train)    
y_test = sc_y.transform(y_test)


# In[196]:


X_train


# In[197]:


X_test


# In[198]:


def create_spfnet(n_layers, n_activation, kernels):
  model = tf.keras.models.Sequential()
  for i, nodes in enumerate(n_layers):
    if i==0:
      model.add(Dense(nodes, kernel_initializer=kernels, activation=n_activation, input_dim=X_train.shape[1]))
      #model.add(Dropout(0.3))
    else:
      model.add(Dense(nodes, activation=n_activation, kernel_initializer=kernels))
      #model.add(Dropout(0.3))
  
  model.add(Dense(1))
  model.compile(loss='mse', 
                optimizer='adam',
                metrics=[tf.keras.metrics.RootMeanSquaredError()])
  return model


# In[199]:


spfnet = create_spfnet([32, 64], 'relu', 'normal')
spfnet.summary()


# In[200]:


from keras.utils.vis_utils import plot_model
plot_model(spfnet, to_file='spfnet_model.png', show_shapes=True, show_layer_names=True)


# In[201]:


hist = spfnet.fit(X_train, y_train, batch_size=32, validation_data=(X_test, y_test),epochs=4, verbose=2, shuffle=False)


# In[202]:


#history = spfnet.fit(X_train1,y_train1,epochs=25,validation_split=0.1, shuffle=False)


# In[203]:


import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

plt.figure(figsize = (15,7))
plt.title('Progreso del entrenamiento')
plt.plot(hist.history['loss'])
plt.plot(hist.history['val_loss'])
plt.legend(['loss', 'val_loss'])
plt.grid()


# In[204]:


df1=pd.DataFrame()
df1['loss'] = hist.history['loss']
df1['val_loss'] = hist.history['val_loss']
ruta1 = "Ent_Solar.csv"
df1.to_csv(ruta1)


# In[205]:


#plt.plot(hist.history['root_mean_squared_error'])
#plt.plot(hist.history['val_root_mean_squared_error'])
#plt.title('Root Mean Squares Error')
#plt.xlabel('Epochs')
#plt.ylabel('error')
#plt.show()


# In[206]:


spfnet.evaluate(X_train, y_train)


# In[207]:


from sklearn.metrics import mean_squared_error

y_pred = spfnet.predict(X_test) # get model predictions (scaled inputs here)
y_pred_orig = sc_y.inverse_transform(y_pred) # unscale the predictions
y_test_orig = sc_y.inverse_transform(y_test) # unscale the true test outcomes

RMSE_orig = mean_squared_error(y_pred_orig, y_test_orig, squared=False)
RMSE_orig


# In[208]:


train_pred = spfnet.predict(X_train) # get model predictions (scaled inputs here)
train_pred_orig = sc_y.inverse_transform(train_pred) # unscale the predictions
y_train_orig = sc_y.inverse_transform(y_train) # unscale the true train outcomes

mean_squared_error(train_pred_orig, y_train_orig, squared=False)


# In[209]:


from sklearn.metrics import r2_score
r2_score(y_pred_orig, y_test_orig)


# In[210]:


r2_score(train_pred_orig, y_train_orig)


# In[211]:


np.concatenate((train_pred_orig, y_train_orig), 1)


# In[212]:


np.concatenate((y_pred_orig, y_test_orig), 1)


# In[213]:


#plt.figure(figsize=(16,6))
#plt.subplot(1,2,2)
#plt.scatter(y_pred_orig, y_test_orig)
#plt.xlabel('Predicted Generated Power on Test Data')
#plt.ylabel('Real Generated Power on Test Data')
#plt.title('Test Predictions vs Real Data')
#plt.scatter(y_test_orig, sc_X.inverse_transform(X_test)[:,2], color='green')
#plt.subplot(1,2,1)
#plt.scatter(train_pred_orig, y_train_orig)
#plt.xlabel('Predicted Generated Power on Training Data')
#plt.ylabel('Real Generated Power on Training Data')
#plt.title('Training Predictions vs Real Data')
#plt.show()


# In[214]:


#x_axis = sc_X.inverse_transform(X_train)[:,-1]
#x2_axis = sc_X.inverse_transform(X_test)[:,-1]
#plt.figure(figsize=(16,6))
#plt.subplot(1,2,1)
#plt.scatter(x_axis, y_train_orig, label='Real Generated Power')
#plt.scatter(x_axis, train_pred_orig, c='red', label='Predicted Generated Power')
#plt.ylabel('Predicted and real Generated Power on Training Data')
#plt.xlabel('Solar Azimuth')
#plt.title('Training Predictions vs Solar Azimuth')
#plt.legend(loc='lower right')

#plt.subplot(1,2,2)
#plt.scatter(x2_axis, y_test_orig, label='Real Generated Power')
#plt.scatter(x2_axis, y_pred_orig, c='red', label='Predicted Generated Power')
#plt.ylabel('Predicted and real Generated Power on TEST Data')
#plt.xlabel('Solar Azimuth')
#plt.title('TEST Predictions vs Solar Azimuth')
#plt.legend(loc='lower right')
#plt.show()


# In[215]:


results = np.concatenate((y_test_orig, y_pred_orig), 1)
results = pd.DataFrame(data=results,index=test2.index[:])
results.columns = ['Produced', 'Predicted']
#results = results.sort_values(by=['Real Solar Power Produced'])
pd.options.display.float_format = "{:,.2f}".format
#results[800:820]
results[7:18]


# In[216]:


results[0:23]


# In[217]:


#results.plot(figsize=(16,7), xlabel='Fecha', ylabel='Comp', grid=True, title='Datos de comp')
#print()


# In[218]:


predicho = results.loc[:,['Predicted']]
real = results.loc[:,['Produced']]
fechas = test2.index[:]
df=pd.DataFrame(index=fechas)
#df['Fechas'] = fechas
df['Real'] = real['Produced'].values
df['Predicho'] = predicho['Predicted'].values
dias_a_graficar = 1
ruta = "Pre_Solar.csv"
df.to_csv(ruta)


plt.figure(figsize=(17,7))


fechas = fechas[0:24*dias_a_graficar]
plt.plot(fechas,predicho[0:24*dias_a_graficar])
plt.plot(fechas,real[0:24*dias_a_graficar])
plt.title('Resultados')
plt.xlabel("Fecha")
plt.ylabel("Potencia (kW)")

plt.legend(['Prediccion', 'Real'])
plt.grid()


# In[219]:


from sklearn.metrics import mean_squared_error
print("MSE: ", mean_squared_error(predicho,real))


# In[220]:


df


# In[ ]:




